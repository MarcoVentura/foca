import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the CrastoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crasto',
  templateUrl: 'crasto.html',
})
export class CrastoPage {

  public crasto;
  public meals;

  constructor(public navCtrl: NavController,  public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrastoPage');
    this.getLocalData();
  }

  getLocalData() {
    this.http.get('assets/data/crasto.json').map(res => res.json()).subscribe(data => {
      this.crasto = data;
      this.meals = this.crasto['crasto'];
      console.log(this.meals)
      console.log(this.crasto);

    });
  }
}
