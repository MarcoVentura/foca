import { Component} from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-santiago',
  templateUrl: 'santiago.html',
})
export class SantiagoPage{

  public santiago;
  public meals;

  constructor(public navCtrl: NavController, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SantiagoPage');
    this.getLocalData();
  }

  getLocalData()
  {
    this.http.get('assets/data/santiago.json').map(res => res.json()).subscribe(data => {
      this.santiago = data;
      this.meals = this.santiago['sant'];
      console.log(this.meals)
      console.log(this.santiago);

    });
  }
}
