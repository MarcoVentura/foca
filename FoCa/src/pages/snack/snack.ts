import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the SnackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-snack',
  templateUrl: 'snack.html',
})
export class SnackPage {

  public snack;
  public meals;

  constructor(public navCtrl: NavController, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SnackPage');
    this.getLocalData();
  }

  getLocalData() {
    this.http.get('assets/data/snack.json').map(res => res.json()).subscribe(data => {
      this.snack = data;
      this.meals = this.snack['snack'];
      console.log(this.meals)
      console.log(this.snack);

    });
  }

}
