import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { SantiagoPage} from "../santiago/santiago";
import { CrastoPage} from "../crasto/crasto";
import { SnackPage} from "../snack/snack"

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SantiagoPage;
  tab3Root = CrastoPage;
  tab4Root = SnackPage;

  constructor() {

  }


}
